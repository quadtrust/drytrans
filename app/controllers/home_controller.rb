class HomeController < ApplicationController
  def index
  end

  def about_us
  end

  def moisture
  end

  def experience
  end

  def contact
  end

  def faq
  end

  def mms90
  end

  def tr5
  end

  def tr20
  end

  def tr50
  end

  def solutions
  end

end
