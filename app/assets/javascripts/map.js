function initMap() {
  var arab = new google.maps.LatLng(25.3463, 55.4209);
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 4,
    center: arab
  });
  var marker = new google.maps.Marker({
    position: arab,
    map: map
  });
}
