// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require twitter/bootstrap
//= require map
//= require viewportchecker

$(document).ready(function(){
    $(".dropdown, .btn-group").hover(function(){
        var dropdownMenu = $(this).children(".dropdown-menu");
        if(dropdownMenu.is(":visible")){
            dropdownMenu.parent().toggleClass("open");
        }
    });
    $(document).on('click','#email-submit', function(e) {
      e.preventDefault();
      if((!$('#email-field').val()) || (!$('#name-field').val())) {
        $('#form-empty-warning').css({'display': 'block'});
        $('#first-warn-text').text('Please fill up all the fields');
        $('#second-warn-text').text('');
      } else {
        var formEmail = $('#email-field').val();
        var formName =  $('#name-field').val();
        var smallFormDetails = {};
        smallFormDetails.email = formEmail;
        smallFormDetails.name = formName;
        console.log('Small form details', smallFormDetails);
        $('#form-empty-warning').css({'display': 'block'});
        $('#first-warn-text').text('Thank you!');
        $('#second-warn-text').text('we will get in touch with you soon.');
      }
    });
    $(document).on('click', '#warn-close', function() {
      $('#form-empty-warning').css({'display': 'none'});
    });

    $(document).on('click', '#contact-form-submit', function(e) {
      console.log('Button clicked');
      e.preventDefault();
      if((!$('#contact-email-field').val()) || (!$('#contact-name-field').val())) {
        $('#form-empty-warning').css({'display': 'block'});
        $('#first-warn-text').text('Please fill up all the fields');
        $('#second-warn-text').text('');
      } else {
        $('#form-empty-warning').css({'display': 'block'});
        $('#first-warn-text').text('Thank you for contacting us!!');
        $('#second-warn-text').text('We will get in touch with you shortly.');

        var email = $('#contact-email-field').val();
        var name =  $('#contact-name-field').val();
        var message = $('#contact-message-field').val();
        var phone = $('#contact-phone-field').val();
        var company = $('#contact-company-field').val();
        var contactDetails = {};
        contactDetails.email = email;
        contactDetails.name = name;
        contactDetails.message = message;
        contactDetails.phone = phone;
        contactDetails.company = company;
        contactDetails.contactDetails = contactDetails;
        console.log('details', contactDetails);
        // alert(contactDetails);
      }
    });
});
